<?php get_header(); ?>
<?php

// check if the repeater field has rows of data
if( have_rows('page_sections_repeater') ):

 	// loop through the rows of data
    while ( have_rows('page_sections_repeater') ) : the_row(); ?>

        <div class="section bg_cover <?php the_sub_field('page_section_class'); ?>" style="background-image:url('<?php the_sub_field('page_section_background');?>')">
			<div class="section_content">
				<div class="section_content <?php the_sub_field('page_section_orientation'); ?>">
					<div class="section_content_main ">
						<div class="section_content_text">
							<?php if( get_sub_field('page_section_title') ): ?>
								<h2 class="section_title"><?php the_sub_field('page_section_title'); ?></h2>
							<?php endif; ?>
							<?php if( get_sub_field('page_section_tagline') ): ?>
								<em class="section_tagline"><?php the_sub_field('page_section_tagline'); ?></em>
							<?php endif; ?>
							<?php if( get_sub_field('page_section_text') ): ?>
								<div class="section_text"><?php the_sub_field('page_section_text'); ?></div>
							<?php endif; ?>
							<?php if( get_sub_field('page_section_button') ): ?>
								<button class="section_button"><?php the_sub_field('page_section_button'); ?></button>
							<?php endif; ?>
						</div>
						<?php if( get_sub_field('page_section_ribbon') ): ?>
						<div class="section_content_ribbons">
							<img class="section_image" src="<?php the_sub_field('page_section_ribbon'); ?>" alt="section_ribbon">
						</div>
						<?php endif; ?>
					</div>
					<?php if( get_sub_field('page_section_image') ): ?>
					<div class="section_content_image">
							<img class="section_image" src="<?php the_sub_field('page_section_image'); ?>" alt="section_image">
					</div>
					<?php endif; ?>
				</div>

			</div>
		</div>

   <?php endwhile;

else :

    // no rows found

endif;

?>
	<div class="section">
		<!-- section -->
		<div class="section_content">


	</div>
	<!-- /section -->
</div>


<?php get_footer(); ?>
